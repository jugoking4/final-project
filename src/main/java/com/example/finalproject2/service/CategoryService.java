package com.example.finalproject2.service;

import com.example.finalproject2.entity.CategoryEntity;
import com.example.finalproject2.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryService {
    @Autowired
    CategoryRepository categoryRepository;
    @Transactional
    public List<CategoryEntity> gCategoryEntityList(){
        List<CategoryEntity> categoryList = (List<CategoryEntity>) categoryRepository.findAll();
        if(!CollectionUtils.isEmpty(categoryList)){
            return categoryList;
        }
        return new ArrayList<>();
    }
}

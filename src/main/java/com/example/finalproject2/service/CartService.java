package com.example.finalproject2.service;

import com.example.finalproject2.entity.CartEntity;
import com.example.finalproject2.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.hibernate.Hibernate;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class CartService {
    @Autowired
    CartRepository cartRepository;
    @Transactional
    public List<CartEntity> showAllCart(){
        List<CartEntity> cartList = (List<CartEntity>) cartRepository.findAll();
        for (CartEntity c :cartList){
            Hibernate.initialize(c.getCartDetailsEntities());
        }
        if (CollectionUtils.isEmpty(cartList)){
            return cartList;
        }
        return new ArrayList<>();
    }
}

package com.example.finalproject2.service;

import com.example.finalproject2.entity.ProductEntity;
import com.example.finalproject2.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;
    @Transactional
    public List<ProductEntity> getProductList(){
        List<ProductEntity> getProduct = (List<ProductEntity>) productRepository.findAll();
        if(!CollectionUtils.isEmpty(getProduct)){
            return getProduct;
        }
        return new ArrayList<>();
    }
}

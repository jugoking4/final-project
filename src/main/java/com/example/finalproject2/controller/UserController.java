//package com.example.finalproject2.controller;
//
//
//import com.example.finalproject2.entity.ProductEntity;
//import com.example.finalproject2.repository.*;
//import com.example.finalproject2.service.CategoryService;
//import com.example.finalproject2.service.ProductService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@Controller
//@RequestMapping(value = "/")
//public class UserController {
//    @Autowired
//    UserRepository userRepository;
//    @Autowired
//    ProductService productService;
//    @Autowired
//    ProductImagesRepository productImagesRepository;
//    @Autowired
//    CategoryRepository categoryRepository;
//    @Autowired
//    CategoryService categoryService;
//    @Autowired
//    ProductSpecificationRepository productSpecificationRepository;
//    @Autowired
//    ProductRepository productRepository;
//    @RequestMapping(value = "/shop", method = GET)
//    public String getShop(Model model){
//        model.addAttribute("productList",productService.getProductList());
//        return "user/shop";
//    }
//    @RequestMapping(value = "/shopCate", method = GET)
//    public String getCategory(Model model){
//        model.addAttribute("cateList", categoryService.gCategoryEntityList());
//        return "user/home-user";
//
//    }
//
//    @RequestMapping(value = "/search", method = GET)
//    public String search(@RequestParam("searchInput")String searchInput, Model model){
//        List<ProductEntity> resultList;
//        if (searchInput.isEmpty()){
//            resultList = (List<ProductEntity>) productRepository.findAll();
//        } else{
//            resultList = productRepository.findByProNameContainingIgnoreCase(searchInput);
//        }
//
//        model.addAttribute("productList", resultList);
//        model.addAttribute("searchInput", searchInput);
//        return "user/shop";
//    }
//
//    @RequestMapping(value = "/category", method = GET)
//    public String listCate(Model model){
//        model.addAttribute("cateList", categoryService.gCategoryEntityList());
//        return "redirect: include/user/header";
//    }
//
//    @RequestMapping(value = "/category/{categoryName}", method = GET)
//    public String cateProductList(@PathVariable String categoryName, Model model){
//        model.addAttribute("productList", productRepository.findByProNameContainingIgnoreCase(categoryName));
//        return "user/shop";
//    }
//
//    @RequestMapping(value = "/viewProduct/{id}", method = GET)
//    public String viewProduct(Model model, @PathVariable long id){
//        model.addAttribute("productList", productService.getProductList());
//        model.addAttribute("product", productRepository.getProductById(id));
//        model.addAttribute("proSpec", productSpecificationRepository.getProSpecById(id));
//        return "user/single-product";
//    }
//
//
//    @GetMapping(value = "/cart")
//    public String showCart(Model model){
//        return "user/cart";
//    }
//
//    @GetMapping(value = "/checkout")
//    public String checkout(Model model){
//        return "user/checkout";
//    }
//
//}

package com.example.finalproject2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class FinalProject2Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(FinalProject2Application.class, args);
    }

}

package com.example.finalproject2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "rating")
public class RatingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int rating;
    private String ratingName;
    private String email;
    private String content;

    @Column(name = "created_date")
    private Date createdDate;
    private String status;
    @ManyToOne
    private ProductEntity productEntity;
}

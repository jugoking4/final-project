package com.example.finalproject2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "orders")
public class OrderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Date date;
    private String status;
    private String shipping_address;
    private String customerName;
    private long phone;
    private String email;
    private String note;
    private String payment;
    private double price;

    @OneToOne
    private AccountEntity account;
    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<OrderDetailsEntity> orderDetailEntities;
}

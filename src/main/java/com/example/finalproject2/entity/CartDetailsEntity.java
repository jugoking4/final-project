package com.example.finalproject2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "cart_details")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartDetailsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pro_Id")
    private ProductEntity productEntity;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "card_Id")
    private CartEntity cartEntity;

    @Column(name = "quantity")
    private int quantity;

//    @OneToOne
//    @PrimaryKeyJoinColumn
//    private PaymentEntity paymentEntity;

    @OneToOne
    @PrimaryKeyJoinColumn
    private AccountEntity accountEntity;
}

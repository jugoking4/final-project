package com.example.finalproject2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "product")
public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String proName;
    //ten sp
    private String title;
//  private String availability;
    private String warranty;
    private double price;
    private long promotion;

    @Column(name = "full_description")
    private String fullDescription;

    //
    @Column(name = "available_in_store")
    private int availableInStore;
    private String status;
//    @OneToMany
//    private List<ProductImages> productImages;
//    @OneToMany
//    private List<ProductPromotion> productPromotion;
//    @OneToMany
//    private List<OrderDetails> orderDetails;
}

package com.example.finalproject2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "cart")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cartId")
    private long id;

    @Column(name = "customerName")
    private String customerName;

    @Column(name = "phoneNumber")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Column(name = "order_date")
    private Date orderDate;

    @Column(name = "totalPrice")
    private double totalPrice;

//    @Column(nullable = false,name = "cartStatus")
//    @Enumerated(EnumType.STRING)
//    private CartStatus cartStatus;

//    @OneToMany(mappedBy = "cartEntity",cascade = CascadeType.ALL,orphanRemoval = true)
//    private List<CartDetailsEntity> cartDetailsEntities = new ArrayList<>();


}

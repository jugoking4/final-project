//package com.example.finalproject2.repository;
//
//import com.example.finalproject2.entity.ProductEntity;
//import org.springframework.data.jpa.repository.Modifying;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
//public interface ProductRepository extends CrudRepository<ProductEntity, Long> {
//    List<ProductEntity> findByProNameContainingIgnoreCase(String proName);
//    List<ProductEntity> findByProName(String proName);
//
////    @Transactional
////    @Modifying
////    @Query('select b from ProductEntity b where b.id = ?')
////    List<ProductEntity> getProductById(long id);
//}
